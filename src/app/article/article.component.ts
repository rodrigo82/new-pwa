import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NewsService} from '../api/news.service';
import {Article} from '../api/model/article.model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  article: Article = null;

  constructor(private route: ActivatedRoute, private newsService: NewsService) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.newsService.article.subscribe(value => this.article = value);
    this.newsService.loadArticle(id);
  }

}
