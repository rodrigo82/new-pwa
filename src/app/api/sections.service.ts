import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {Section} from './model/sections.model';
import {generate, newSections} from './model/factory.functions';

@Injectable()
export class SectionsService {

  sections = new Subject<Section[]>();
  section = new Subject<Section>();

  loadSections() {
    this.sections.next(generate(6, newSections));
  }

  loadSection(id: string) {
    this.section.next(generate(1, newSections)[0]);
  }

}
