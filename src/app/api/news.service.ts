import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {News} from './model/news.model';
import {generate, newArticles, newNews} from './model/factory.functions';
import {Article} from './model/article.model';

@Injectable()
export class NewsService {

  mainNews = new Subject<News[]>();
  article = new Subject<Article>();
  sectionNews = new Subject<News[]>();

  loadMainNews() {
    this.mainNews.next(generate(10, newNews));
  }

  loadArticle(id: string) {
    const article = generate(1, newArticles)[0];
    article.id = id;
    this.article.next(article);
  }

  loadSectionNews(sectionId: string) {
    this.sectionNews.next(generate(10, newNews));
  }

}
