import {News} from './news.model';

export interface Article extends News{
  content: string[];
}
