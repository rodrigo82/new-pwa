export interface News {
  id: string;
  title: string;
  date: Date;
  author: string;
  source: String;
  imageUrl: string;
  summary: string;
}
