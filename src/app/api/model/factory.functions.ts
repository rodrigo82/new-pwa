import faker from 'faker';
import {News} from './news.model';
import {Section} from './sections.model';
import {Article} from './article.model';

interface Supplier<T> {
  get(): T;
}

function getImageUrl(): string {
  return 'https://picsum.photos/400/200/?random&' + faker.random.number();
}

export function generate<T>(num: number, supplier: Supplier<T>) {
  return Array.from(new Array(num)).map(v => supplier.get());
}

export const newSections = new class NewSection implements Supplier<Section> {
  get(): Section {
    return {
      name: faker.lorem.word(),
      id: faker.random.uuid()
    };
  }
};

export const newNews = new class NewNews implements Supplier<News> {
  get(): News {
    return {
      id: faker.random.uuid(),
      author: faker.name.findName(),
      date: new Date(),
      source: faker.internet.domainName(),
      title: faker.lorem.sentence(),
      imageUrl: getImageUrl(),
      summary: faker.lorem.sentences(2)
    };
  }
};

export const newArticles = new class NewArticle implements Supplier<Article> {
  get(): Article {
    return {
      id: faker.random.uuid(),
      author: faker.name.findName(),
      date: new Date(),
      content: faker.lorem.paragraphs(5).split('\n'),
      source: faker.internet.domainName(),
      title: faker.lorem.sentence(),
      summary: faker.lorem.sentences(2),
      imageUrl: getImageUrl()
    };
  }
};
