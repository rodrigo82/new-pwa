import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SectionsService} from './sections.service';
import {NewsService} from './news.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [SectionsService, NewsService]
})
export class ApiModule { }
