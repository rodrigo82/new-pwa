import {Component, OnInit} from '@angular/core';
import {Section} from '../api/model/sections.model';
import {SectionsService} from '../api/sections.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  public sections: Section[] = [];

  constructor(private sectionsService: SectionsService) {
  }

  ngOnInit() {
    this.sectionsService.sections.subscribe(value => this.sections = value);
    this.sectionsService.loadSections();
  }

}
