import { Component, OnInit } from '@angular/core';
import {News} from '../api/model/news.model';
import {ActivatedRoute} from '@angular/router';
import {NewsService} from '../api/news.service';
import {Section} from '../api/model/sections.model';
import {SectionsService} from '../api/sections.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {

  news: News[];
  section: Section;

  constructor(private route: ActivatedRoute,
              private newsService: NewsService,
              private sectionsService: SectionsService) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.sectionsService.section.subscribe(value => this.section = value);
    this.sectionsService.loadSection(id);

    this.newsService.sectionNews.subscribe(value => this.news = value);
    this.newsService.loadSectionNews(id);
  }

}
