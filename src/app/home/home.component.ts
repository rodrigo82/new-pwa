import { Component, OnInit } from '@angular/core';
import {NewsService} from '../api/news.service';
import {News} from '../api/model/news.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public news: News[] = [];

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.newsService.mainNews.subscribe(value => this.news = value);
    this.newsService.loadMainNews();
  }

}
