import {Component, Input, OnInit} from '@angular/core';
import {News} from '../../api/model/news.model';

@Component({
  selector: 'app-newscard',
  templateUrl: './newscard.component.html',
  styleUrls: ['./newscard.component.css']
})
export class NewscardComponent {

  @Input()
  news: News;

}
